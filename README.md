This code allows to perform a Hartree fock calculation for H2 molecule and also to obtain 
plotable values for charge density and single electron energies. 

The main structure of the program is a program file, called main.f90, and a series of subroutines
and functions located in separated files that take care of specific parts of the process.
The main program mainly presents the results and call the subroutines.

The file called compile inside fortran folder, is the bash file used to compile all the subroutines, functions and main program. 
The execution includes llapack as well, which is used for performing diagonalization calculations

The parameters for H2 molecule are given with the program in the main file. Different energies can
be computed modifying distances and exponents. The coefficients were taken from reference, Szabo book.

I aknowledge here the charge density values with respect to distances are wrong, but due to lack of time i could not find how to 
obtain it properly. However the change is only of a constant (the result of the integral(phi*phi))

The program delivers three files, e_values.dat, which contains HOMO values, e2_values.dat, which contains LUMO values and 
p_values.dat for charge densities. The first column is for  the distance and the second column is for either the p or e values. 
No text is given so that the plotting is much easier and straighforward.

Every time a calculation is intended to be done, e_values and p_values files must be deleted