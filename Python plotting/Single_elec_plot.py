import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('e_values.dat')
x_values = data[:, 0]
y_values = data[:, 1]

plt.plot(x_values, y_values)
plt.xlabel('Bond distance (a.u.)')
plt.ylabel('HOMO energies (Hartrees)')
plt.show()

