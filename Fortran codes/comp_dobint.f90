 subroutine comp_dobint(nbas, nprim, expon, abcd, dist, coeff)

 implicit none
 integer                           :: k,l,m,n
 integer                           :: nbas, nprim
 double precision                  :: R1, R2, R3, R4, R5
 double precision                  :: twoe, norm
 double precision                  :: coeff(3)
 double precision                  :: abcd(nbas,nbas,nbas,nbas)
 double precision                  :: dist(nbas), expon(nprim)
 double precision, parameter      :: pi = DATAN(1.d0)*4


 ! Calculation of double electron integrals and the respective storage in a variable. Some of the 
 ! integrals have not been computed since they are just equal to one calculated inside the loop. 
 ! Twoe function takes care of the calculation of the integral given specific parameters.
 DO k = 1, nprim
  Do l = 1, nprim
   Do m = 1, nprim
    Do n = 1, nprim
 ! R parameters
   R1 = expon(k)*dist(2)/(expon(k)+expon(l))
   R2 = dist(2)-R1
   R3 = expon(m)*dist(2)/(expon(m)+expon(n)) 
   R4 = dist(2)-R3
   R5 = R1-R3

   abcd(1,1,1,1) = abcd(1,1,1,1) + twoe(expon(k),expon(l),expon(m), expon(n), 0.d0, 0.d0, 0.d0) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
   abcd(2,1,1,1) = abcd(2,1,1,1) +twoe(expon(k),expon(l),expon(m), expon(n), dist(2), 0.d0, r1) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
   abcd(2,1,2,1) = abcd(2,1,2,1) + twoe(expon(k),expon(l),expon(m), expon(n), dist(2), dist(2), R5) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
   abcd(2,2,1,1) = abcd(2,2,1,1) + twoe(expon(k),expon(l),expon(m), expon(n), 0.d0, 0.d0, dist(2)) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
   abcd(2,2,2,1) = abcd(2,2,2,1) + twoe(expon(k),expon(l),expon(m), expon(n), 0.d0, dist(2), R4) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
   abcd(2,2,2,2) = abcd(2,2,2,2) + twoe(expon(k),expon(l),expon(m), expon(n), 0.d0, 0.d0, 0.d0) &
                 * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)) &
                 * norm(coeff(m),expon(m))*norm(coeff(n),expon(n))
  enddo
 enddo
enddo
enddo
 
   abcd(1,2,1,1) = abcd(2,1,1,1)
   abcd(1,2,1,1) = abcd(2,1,1,1)
   abcd(1,1,2,1) = abcd(2,1,1,1)
   abcd(1,1,1,2) = abcd(2,1,1,1)
   abcd(1,2,2,1) = abcd(2,1,2,1)
   abcd(2,1,1,2) = abcd(2,1,2,1)
   abcd(1,2,1,2) = abcd(2,1,2,1)
   abcd(1,1,2,2) = abcd(2,2,1,1)
   abcd(2,2,1,2) = abcd(2,2,2,1)
   abcd(2,1,2,2) = abcd(2,2,2,1)
   abcd(1,2,2,2) = abcd(2,2,2,1)

 END subroutine comp_dobint
