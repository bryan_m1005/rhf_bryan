 subroutine core(T, pot, pot2, Hcore)
 
 implicit none
 integer                :: i,j
 double precision       :: T(2,2), pot(2,2), pot2(2,2), Hcore(2,2)

! Straightforward subroutine. Computation of Hcore matrix
 Hcore = T + pot + pot2
 end subroutine core
