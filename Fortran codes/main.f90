 program RHF_H2
 
 implicit none
 
 integer                          :: i,j
 integer                          :: grid=300
 double precision                 :: Dist(2)
 double precision, allocatable    :: S(:,:)
 double precision                 :: P(2,2)
 double precision, allocatable    :: K(:,:)
 double precision, allocatable    :: Pot(:,:)
 double precision, allocatable    :: Pot2(:,:)
 double precision, allocatable    :: Pot_fin(:,:)
 double precision, allocatable    :: n(:,:)
 double precision, allocatable    :: Hcore(:,:)
 double precision                 :: expon(3)
 double precision                 :: norm
 double precision                 :: E_HF
 double precision                 :: e(2)
 double precision                 :: c(2)
 double precision                 :: normal(2)
 double precision                 :: coeff(3)
 double precision, allocatable    :: abcd(:,:,:,:)
 double precision                 :: n_off(3)
 double precision, parameter      :: pi = DATAN(1.d0)*4
 
 integer, parameter               :: nbas = 2
 integer, parameter               :: nprim = 3

 ! Input parameters of H2

 Dist(1) = 0.d0
 Dist(2) = 1.40106
 expon(1) = 4.3448
 expon(2) = 0.66049
 expon(3) = 0.13669
 coeff(1) = 0.154329
 coeff(2) = 0.535328
 coeff(3) = 0.444635
 
 ! Main program that performs a RHF (spatial orbitals) calculation for H2 molecule.
 
 ! Some things have been tried to be generalized for other cases, However, due to lack of time
 ! some things have been done faster or without the intention of generalize. That explains why 
 ! in some parts there are allocatables whereas in others its already fixed in declarations. A
 ! deeper work on this code would allow to optimize it and polish it to correct such inefficiencies.

 ! For the same reason presented before, it might happen you find declared variables that are not
 ! used in the code, they appeared in debugging processes as help or in previous ideas of implementation.
 ! Even though i tried to get rid of them, due to a lack of time it is possible that I oversaw some
 ! of them

 ! First step is to calculate the integrals. Every matrix is computed in a subroutine located in different files
 

write(*,*) "**********************************************************************************************+*************"
write(*,*) "*                                                                                                          *"
write(*,*) "*                                                                                                          *"
write(*,*) "*                              Hartree Fock calculation for Hydrogen molecule (H2)                         *"
write(*,*) "*                                                                                                          *"
write(*,*) "*                                                                                                          *"
write(*,*) "**********************************************************************************************+*************"

Write(*,*)
Write(*,*)
Write(*,*)

! As it is a calculation for just H2, the needed parameters are introduced here
Write(*,*) "Input parameters:    "
Write(*,*) "Bond distance:    ", Dist(2), "a.u"
Write(*,*) "Basis set size:     STO-3G"
Write(*,*) "Exponents of the gaussians:   ", "alfa1 = ", expon(1), "alfa2 = ", expon(2), "alfa3 = ", expon(3)
Write(*,*) "Contraction coefficients:     ", "coeff1 = ", coeff(1), "coeff2 = ", coeff(2), "coeff3 = ", coeff(3)

Write(*,*)
Write(*,*) "************************************************************************************************************"
!end

Write(*,*)
 allocate(S(nbas,nbas)) 
 call overlap(nbas, nprim, expon, S, dist, n, coeff)

write(*,*) "Calculation of Overlap Matrix (S): "
do i=1,nbas
write(*,*) (S(i,j),j=1,nbas)
enddo
write(*,*)
Write(*,*) "************************************************************************************************************"

Write(*,*)
Write(*,*) "************************************************************************************************************"
Write(*,*)
 allocate(K(nbas,nbas)) 
 call kinetic(nbas, nprim, expon, K, dist, n, coeff)

write(*,*) "Calculation of Kinetic energies matrix (K): "
do i=1,nbas
write(*,*) (K(i,j),j=1,nbas)
enddo
write(*,*)
Write(*,*) "************************************************************************************************************"

Write(*,*) "************************************************************************************************************"
Write(*,*)
 allocate(Pot(nbas,nbas), Pot2(nbas, nbas)) 
 call potential(nbas, nprim, expon, pot, pot2, dist, n, coeff)
 Pot_fin = pot + pot2
write(*,*) "Calculation of Potential energies matrix (V) = (V1A+V1B): "
do i=1,nbas
write(*,*) (Pot_fin(i,j),j=1,nbas)
enddo
write(*,*)
Write(*,*) "************************************************************************************************************"

 
Write(*,*) "************************************************************************************************************"
Write(*,*)
 allocate(Hcore(nbas,nbas), abcd(nbas,nbas,nbas,nbas))
 call comp_dobint(nbas, nprim, expon, abcd, dist,coeff)
 call core(K, pot, pot2, Hcore) 

write(*,*) "Calculation of the Ecore matrix (one electron terms): "
do i=1,nbas
write(*,*) (Hcore(i,j),j=1,nbas)
enddo
write(*,*)
Write(*,*) "************************************************************************************************************"


Write(*,*)
Write(*,*)
write(*,*) 
write(*,*) 
Write(*,*) "************************************************************************************************************"

write(*,*) 
write(*,*)  "Initializing Hartree-Fock method. Convergence criteria 1.D-06"
write(*,*)
Write(*,*) "************************************************************************************************************"
Write(*,*)
Write(*,*)

 call RHF(S, K, Pot+Pot2, Hcore, abcd, E_HF, e, c, P)







 Call plotting_ener(grid)
 END program RHF_H2
