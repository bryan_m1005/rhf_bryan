subroutine RHF1(S, T, V, Hcore, abcd, E_HF, e, cmat, P)
! A copy pase of RHF subroutine but without all the print instructions of the former one. Is used so
! when computing the grid the terminal is not flooded with a lot of text resulting of the 
! recursive call of RHF
 implicit none

 integer                       :: k, l, m, n, i, j
 integer                       :: nIt
 integer, parameter            :: grid = 300
 integer                       :: Maxit = 4
 integer                       :: Thresh = 1.d-6
 double precision              :: S(2, 2)
 double precision              :: T(2, 2)
 double precision              :: V(2, 2)
 double precision              :: Hcore(2, 2)
 double precision              :: abcd(2, 2, 2, 2)
 double precision              :: E_HF
 double precision              :: e(2)
 double precision              :: Sol(2, 2)
 double precision              :: Gmat(2, 2)
 double precision              :: S_inv(2, 2)
 double precision              :: S_ort(2, 2)
 double precision              :: S_ortin(2, 2)
 double precision              :: eigen_s(2)
 double precision              :: Fprim(2,2)
 double precision              :: Cmatp(2,2)
 double precision              :: Cmat(2,2)
 double precision              :: P(2,2)
 double precision              :: P_old(2,2)
 double precision              :: P_new(2,2)
 double precision              :: Jmat(2,2)
 double precision              :: Kmat(2,2)
 double precision              :: F(2,2)
 double precision              :: err(2,2)
 double precision              :: conv
 double precision              :: E_kin_inter(2,2)
 double precision              :: E_kin
 double precision              :: E_pot_inter(2,2)
 double precision              :: E_pot
 double precision              :: E_coul_inter(2,2)
 double precision              :: E_coul
 double precision              :: E_int_inter(2,2)
 double precision              :: E_int
 DOUBLE PRECISION                      :: work(2*3)
 integer                       :: lwork, info



 nIt = 0
 Conv = 1.d0

 lwork = size(work)
 
 S_ort(:,:) = 0.d0

 S_ort(1,1) = 1.d0/dsqrt(2.d0*(1.d0+S(1,2)))
 S_ort(2,1) = s_ort(1,1)
 S_ort(1,2) = 1.d0/dsqrt(2.d0*(1.d0-S(1,2)))
 S_ort(2,2) = -S_ort(1,2)


 P(:,:) = 0.d0 

 do while (Conv > thresh .and. nIt < maxIt)
  nIt = nIt + 1
 Gmat = 0.d0
 do i = 1, 2
  do j = 1, 2
   do k = 1, 2
    do l = 1, 2

     Gmat(i,j) = Gmat(i,j) + P(k,l)*(abcd(i,j,k,l)-0.5d0*abcd(i,l,k,j))

     enddo
     enddo
    enddo
   enddo

 F = Hcore + Gmat
 E_HF=0.d0
 do i=1,2
  do j=1,2
 E_HF = E_HF + 0.5d0*P(i,j)*(Hcore(i,j)+F(i,j))
 enddo
 enddo
 
 Fprim = matmul(transpose(S_ort),matmul(F,S_ort))

 Cmatp(:,:) = Fprim(:,:)

 call dsyev('V', 'U', 2, Cmatp, 2, e , work, lwork, info)
 
 Cmat = matmul(S_ort, Cmatp)
 P_old = P(:,:) 
 P = 0.d0
 do i = 1,2
  do j = 1,2
   do k = 1,1
     p(i,j) = P(i,j) + 2.d0 * Cmat(I,k)*Cmat(J,k)
   enddo
  enddo
 enddo
 conv = 0.d0 
 do i=1,2
  do j=1,2
   conv = conv +(P(i,j)-P_old(i,j))**2.d0
  enddo
 enddo
   conv = dsqrt(conv/4.d0)

 enddo
 

End subroutine RHF1
