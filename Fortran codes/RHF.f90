subroutine RHF(S, T, V, Hcore, abcd, E_HF, e, cmat, P)

 implicit none

 integer                       :: k, l, m, n, i, j
 integer                       :: nIt
 integer, parameter            :: grid = 300
 integer                       :: Maxit = 4
 integer                       :: Thresh = 1.d-6
 double precision              :: S(2, 2)
 double precision              :: T(2, 2)
 double precision              :: V(2, 2)
 double precision              :: Hcore(2, 2)
 double precision              :: abcd(2, 2, 2, 2)
 double precision              :: E_HF
 double precision              :: e(2)
 double precision              :: Sol(2, 2)
 double precision              :: Gmat(2, 2)
 double precision              :: S_inv(2, 2)
 double precision              :: S_ort(2, 2)
 double precision              :: S_ortin(2, 2)
 double precision              :: eigen_s(2)
 double precision              :: Fprim(2,2)
 double precision              :: Cmatp(2,2)
 double precision              :: Cmat(2,2)
 double precision              :: P(2,2)
 double precision              :: P_old(2,2)
 double precision              :: P_new(2,2)
 double precision              :: Jmat(2,2)
 double precision              :: Kmat(2,2)
 double precision              :: F(2,2)
 double precision              :: err(2,2)
 double precision              :: conv
 double precision              :: E_kin_inter(2,2)
 double precision              :: E_kin
 double precision              :: E_pot_inter(2,2)
 double precision              :: E_pot
 double precision              :: E_coul_inter(2,2)
 double precision              :: E_coul
 double precision              :: E_int_inter(2,2)
 double precision              :: E_int
 DOUBLE PRECISION                      :: work(2*3)
 integer                       :: lwork, info


write(*,*) "                                    .....................                   "
write(*,*) 
write(*,*) 

 ! Starts setting up the SCF parameters
 nIt = 0
 Conv = 1.d0

 lwork = size(work)
 
 ! Calculating the S^-1/2 matrix using the procedure described in the reference book
 S_ort(:,:) = 0.d0

 S_ort(1,1) = 1.d0/dsqrt(2.d0*(1.d0+S(1,2)))
 S_ort(2,1) = s_ort(1,1)
 S_ort(1,2) = 1.d0/dsqrt(2.d0*(1.d0-S(1,2)))
 S_ort(2,2) = -S_ort(1,2)

 !initial guess

 P(:,:) = 0.d0 

 !Here begins the SCF loop
 do while (Conv > thresh .and. nIt < maxIt)

 ! Calculation of G matrix that accounts for the two electron terms

  nIt = nIt + 1
 Gmat = 0.d0
 do i = 1, 2
  do j = 1, 2
   do k = 1, 2
    do l = 1, 2

     Gmat(i,j) = Gmat(i,j) + P(k,l)*(abcd(i,j,k,l)-0.5d0*abcd(i,l,k,j))

     enddo
     enddo
    enddo
   enddo

 ! Fock operator
 F = Hcore + Gmat
 E_HF=0.d0
 do i=1,2
  do j=1,2
 E_HF = E_HF + 0.5d0*P(i,j)*(Hcore(i,j)+F(i,j))
 enddo
 enddo
 
 !Transformation of F to F' in order to compute the new P matrix 
 Fprim = matmul(transpose(S_ort),matmul(F,S_ort))

 Cmatp(:,:) = Fprim(:,:)
 
 ! Llapack subroutine is used for diagonalization procedures
 call dsyev('V', 'U', 2, Cmatp, 2, e , work, lwork, info)
 
 Cmat = matmul(S_ort, Cmatp)
 P_old = P(:,:) 
 P = 0.d0
 do i = 1,2
  do j = 1,2
   do k = 1,1
     p(i,j) = P(i,j) + 2.d0 * Cmat(I,k)*Cmat(J,k)
   enddo
  enddo
 enddo

! Same convergence criteria than the reference
 conv = 0.d0 
 do i=1,2
  do j=1,2
   conv = conv +(P(i,j)-P_old(i,j))**2.d0
  enddo
 enddo
   conv = dsqrt(conv/4.d0)

 write(*,*) "Cycle ", nIt, "       HF energy: ", E_HF, "Convergence: ", conv
Write(*,*) "************************************************************************************************************"
 enddo
 
write(*,*) 
write(*,*) 
Write(*,*) "************************************************************************************************************"
write(*,*) 
write(*,*) 
write(*,*) "                 Results Converged. SCF process finished successfully       "        
write(*,*) 
write(*,*) "Orbital energies:"
Do i=1,2
write(*,*) "MO", i, "        Energy", e(i)
enddo
write(*,*) 
write(*,*) 
Write(*,*) "************************************************************************************************************"

End subroutine RHF
