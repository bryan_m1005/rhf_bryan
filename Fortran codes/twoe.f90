 double precision function twoe(alfa, beta, gama, delta, dif1, dif2, dif3 )
 implicit none
 double precision                  :: alfa, beta, gama, delta
 double precision                  :: dif1, dif2, dif3, fun
 double precision, parameter      :: pi = DATAN(1.d0)*4

 ! for computing the two electron calculating using primitives. The fun function is used in the same
 ! way done for potential subroutine
 twoe = 2.d0*(pi**(5.d0/2.d0))/((alfa+beta)*(gama+delta) &
      * dsqrt(alfa+beta+gama+delta)) &
      * fun((alfa+beta)*(gama+delta)*dif3**2.d0/(alfa+beta+gama+delta)) &
      * dexp(-alfa*beta*dif1**2.d0/(alfa+beta)-gama*delta*dif2**2.d0/(gama+delta))
 end function twoe
