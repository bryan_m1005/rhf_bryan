 subroutine overlap(nbas, nprim, expon, S, dist, n, coeff)
 
 implicit none
 integer                          :: i , j, k, l
 integer                          :: nbas, nprim
 double precision, parameter      :: pi = 3.141592653590
 double precision, intent(in)     :: expon(nprim) 
 double precision, intent(inout)  :: S(nbas, nbas) 
 double precision, intent(in)     :: dist(2)
 double precision, intent(in)     :: n(nbas, nprim)
 double precision                 :: norm
 double precision                 :: coeff(3)
 double precision                 :: con
 
 
 !This loop takes care of the overlap calculation. Function norm multiplies the contraction coefficients
! with the normalization constants so the loops can be calculated for unnormalized gaussians 
! All the subroutines called Overlap, Kinetic, Potential and comp_dobint use the formulas given in
! the appendix A of Szabo book for computing the integrals
 Do i = 1,nbas
  do j = 1,nbas
   S(i,j) = 0.d0
   do k=1,nprim
    do l=1,nprim
     if (i==j) then 
      S(i,j) = S(i,j) + (pi/(expon(k)+expon(l)))**(3.d0/2.d0)*(dexp(-((expon(k)*expon(l))/(expon(k)+expon(l)))  &
             * (dist(1)-dist(1))**2))  &
             * norm(coeff(k),expon(k))*norm(coeff(l),expon(l))
 ! These two scenarios within the conditional its just to account for when r1-r2 is 0 and when not,
 ! which depends on the indexes of the loop
     else
      S(i,j) = S(i,j) + (pi/(expon(k)+expon(l)))**(3.d0/2.d0)*(dexp(-((expon(k)*expon(l))/(expon(k)+expon(l)))  &
             * (dist(1)-dist(2))**2))  &
             * norm(coeff(k),expon(k))*norm(coeff(l),expon(l))
     endif
    enddo
    enddo
   enddo
  enddo
  con=s(1,1)
 
 ! The previous loop does not provide diagonal values equal to 1. I suspect is because the choose of 
!contraction coefficients, which i took from Szabo book. However, I did not know how to find the proper
 !ones, hence i make this loop to normalize it. The difference between S after this loop and the 
 !previous one that the precision would not be affected considerable
  do i=1,nbas
   do j=1,nbas
    S(i,j)=S(i,j)/con
   enddo
  enddo

 END subroutine overlap
