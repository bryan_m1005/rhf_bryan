 subroutine potential(nbas, nprim, expon, pot, pot2, dist, n, coeff)

 implicit none
 integer                          :: i , j, k, l
 integer                          :: nbas, nprim
 double precision, parameter      :: pi = DATAN(1.d0)*4
 double precision, intent(in)     :: expon(nprim) 
 double precision, intent(inout)  :: Pot(nbas, nbas) 
 double precision                 :: Pot2(nbas, nbas) 
 double precision                 :: fun
 double precision                 :: coeff(3)
 double precision, intent(in)     :: dist(2)
 double precision, intent(in)     :: n(nbas, nprim)
 double precision                 :: norm, normal(2)

 !Here, two matrices have to be calculated. So each of the loops take care of computing one of them. 
 ! There is also a conditional here that takes into account the values of Rs depending on the indexes
 !Here however another case is introduced accounting the new R computed
 ! fun function takes care of the error function. This part is heavily based on the appendix, since 
 ! using directly the built in erf function directly seems to be incorrect and I did not know why
 Do i = 1,nbas
  do j = 1,nbas
   Pot(i,j) = 0.d0
   do k=1,nprim
    do l=1,nprim
     if ((i==j).and.(i==1)) then 
      Pot(i,j) = Pot(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               * fun((expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               *  norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))

     else if ((i==j).and.(i==2)) then 
      Pot(i,j) = Pot(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               * fun((expon(k)+expon(l))*(dist(1)-dist(2))**2.d0) &
               *  norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))
     else

      Pot(i,j) = Pot(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(2))**2.d0) &
               * fun((expon(k)+expon(l))*((expon(l)*dist(2))/(expon(k)+expon(l)))**2.d0) &
               * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))

     endif

 
    enddo
    enddo
   enddo
  enddo


 Do i = 1,nbas
  do j = 1,nbas
   Pot2(i,j) = 0.d0
   do k=1,nprim
    do l=1,nprim
     if ((i==j).and.(i==1)) then 
      Pot2(i,j) = Pot2(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               * fun((expon(k)+expon(l))*(dist(1)-dist(2))**2.d0) &
               * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))

      
     else if ((i==j).and.(i==2)) then 
      Pot2(i,j) = Pot2(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               * fun((expon(k)+expon(l))*(dist(1)-dist(1))**2.d0) &
               * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))
     else

      Pot2(i,j) = Pot2(i,j) + (-2.d0*pi/(expon(k)+expon(l)) &
               * dexp(-expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(2))**2.d0) &
               * fun((expon(k)+expon(l))*(dist(2)-((expon(l)*dist(2))/(expon(k)+expon(l))))**2.d0) &
               * norm(coeff(k),expon(k))*norm(coeff(l),expon(l)))
      

     endif

 
    enddo
    enddo
   enddo
  enddo

 END subroutine potential
