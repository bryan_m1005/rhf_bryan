 double precision Function fun(x)

 implicit none
 double precision                :: x
 double precision, parameter     :: pi = datan(1.d0)*4.d0

 if (x <= 1.D-06) then
  fun = 1.d0 - x/2.d0
 else
  fun = dsqrt(pi/x)*erf(dsqrt(x))/2.d0
 endif
 return
 end function fun
