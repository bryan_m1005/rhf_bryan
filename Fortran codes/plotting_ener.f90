 subroutine plotting_ener(grid)
! This is basically just a copy paste of the main program in main.f90. The reason for this I forgot
! the goal was to obtain plottable values so I was focused on the SCF procedure.
! Further developments would enable to improve it and optimize it.
! The difference here is the bond distance is modified so a grid can be computed. The grid is a 
! parameter 
 implicit none
 
 integer                          :: i,j, h, l
 integer                          :: grid
 double precision                 :: Dist(2)
 double precision                 :: accum
 double precision                 :: S(2,2)
 double precision                 :: K(2,2)
 double precision                 :: Pot(2,2)
 double precision                 :: Pot2(2,2)
 double precision                 :: n(2,2)
 double precision                 :: Pot_fin(2,2)
 double precision                 :: P(2,2)
 double precision                 :: Hcore(2,2)
 double precision                 :: expon(3)
 double precision                 :: norm
 double precision                 :: E_HF
 double precision                 :: e(2)
 double precision                 :: c(2)
 double precision                 :: normal(2)
 double precision                 :: coeff(3)
 double precision                 :: abcd(2,2,2,2)
 double precision                 :: n_off(3)
 double precision, parameter      :: pi = DATAN(1.d0)*4
 
 integer, parameter               :: nbas = 2
 integer, parameter               :: nprim = 3

 ! Input parameters of H2


 open(unit=22, file='e_values.dat' )
 open(unit=33, file='p_values.dat' )
 open(unit=44, file='e2_values.dat' )


Do i=1, grid

 Dist(1) = 0.d0
 Dist(2) = 0.01d0 + (dble(i)*3.0d0/dble(grid))
 expon(1) = 4.3448
 expon(2) = 0.66049
 expon(3) = 0.13669
 coeff(1) = 0.154329
 coeff(2) = 0.535328
 coeff(3) = 0.444635
 
 S = 0.d0
 call overlap(nbas, nprim, expon, S, dist, n, coeff)
 K = 0.d0
 call kinetic(nbas, nprim, expon, K, dist, n, coeff)
 Pot = 0.d0
 Pot2 = 0.d0
 call potential(nbas, nprim, expon, pot, pot2, dist, n, coeff)
 Hcore = 0.d0
 abcd = 0.d0
 call comp_dobint(nbas, nprim, expon, abcd, dist,coeff)
 call core(K, pot, pot2, Hcore) 
 e = 0.d0
 E_HF = 0.d0
 c = 0.d0
 call RHF1(S, K, Pot+Pot2, Hcore, abcd, E_HF, e, c, P)


 write(22,*) Dist(2), e(1)
 write(44,*) Dist(2), e(2)
 accum = 0.d0
 do h=1,2
  do l=1,2
   accum = accum + P(h,l)
  enddo
 enddo 
 write(33,*) Dist(2), accum
enddo
 close(22)
 close(33)
 close(44)
 END subroutine plotting_ener
