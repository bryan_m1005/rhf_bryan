 subroutine kinetic (nbas, nprim, expon, kin, dist, n, coeff)

 implicit none
 integer                          :: i , j, k, l
 integer                          :: nbas, nprim
 double precision, parameter      :: pi = DATAN(1.d0)*4
 double precision, intent(in)     :: expon(nprim) 
 double precision, intent(inout)  :: Kin(nbas, nbas) 
 double precision, intent(in)     :: dist(2)
 double precision, intent(in)     :: n(nbas, nprim)
 double precision                 :: norm
 double precision                 :: coeff(3)
 
 !Same case here than for overlap, there is a conditional that separes the expressions depending on the values of r1-r2
 Do i = 1,nbas
  do j = 1,nbas
   Kin(i,j) = 0.d0
   do k=1,nprim
    do l=1,nprim
     if (i==j) then 
      Kin(i,j) = kin(i,j) + (expon(k)*expon(l))/(expon(k)+expon(l)) &
               * (3.d0) &
               * ((pi/(expon(k)+expon(l)))**(3.d0/2.d0))  &
               *  norm(coeff(k),expon(k))*norm(coeff(l),expon(l))
      
     else
      
      Kin(i,j) = kin(i,j) + (expon(k)*expon(l))/(expon(k)+expon(l)) &
               * (3.d0-(2.d0*expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(2))**2.d0)) &
               * ((pi/(expon(k)+expon(l)))**(3.d0/2.d0)) &
               * dexp(- expon(k)*expon(l)/(expon(k)+expon(l))*(dist(1)-dist(2))**2.d0) &
               *  norm(coeff(k),expon(k))*norm(coeff(l),expon(l))
     endif
    enddo
    enddo
   enddo
  enddo

 
 END subroutine kinetic
