 double precision function  norm(coeff,alfa)

 implicit none
 double precision sq_fun, alfa , coeff
 double precision, parameter      :: pi = 3.141592653590

 ! computes the multiplication of the contracction coefficient with the normalization constant
      norm= coeff * ((2.d0*alfa/pi)**(3.d0/4.d0))
     
 end function norm
